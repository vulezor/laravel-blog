<?php

use App\Post;
use App\Photo;
use App\User;
use App\Country;

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/about', function () {
//     return 'Hi About Page';
// });
// Route::get('/contact', function () {
//     return 'Hi Contact Page';
// });

// Route::get('/post/{id}', function ($id) {
//     return 'Hi Post Page = '.$id;
// });

// Route::get('admin/posts/example', array('as'=>'admin.home', function(){
//     $url = route('admin.home');
//     return "this is ".$url;
// }));



//Route::get('/post/{id}', 'PostsController@index');

//Route::resource('posts', 'PostsController');

// Route::get('/contact', 'PostsController@contact'); 

// Route::get('post/{id}/{name}/{password}', 'PostsController@show_post');


// Route::get('/insert', function(){
//     DB::insert('insert into posts(title, content) values (?, ?)', ['PHP with Laravevl', 'Laravel is the best thing that has happen to php world']);
// });

// Route::get('/read', function(){
//     $results = DB::select('SELECT * FROM posts WHERE id=?', [1]);
//     return var_dump($results);
//     // foreach($results as $result){
//     //     print_r($result->title);
//     // }
    
// });

// Route::get('/update', function(){
//     $updated = DB::update('UPDATE posts set title="Update title" WHERE id=?', [1]);
//     return $updated;
// });
// Route::get('/delete', function(){
//     $deleted = DB::delete('DELETE FROM posts WHERE id=?', [1]);
//     return $deleted;
// });
/*
|--------------------------------------------------------------------------
| Eloquent
|--------------------------------------------------------------------------
|*/

// Route::get('/read', function(){
//     $posts = Post::all();
//     foreach($posts as $post){
//         return $post->title;
//     }
// });

// Route::get('/find/{id}', function($id){
//     $post = Post::find($id);
//     return $post->title;
// });

// Route::get('/findwhere/{id}', function($id){
//     $posts = Post::where('id', $id)->orderBy('id', 'desc')->take(1)->get();
//     return $posts;
// });

// Route::get('/findorfail/{id}', function($id){
//     $posts = Post::findOrFail($id);
//     return $posts;
// });

// Route::get('/findorfalse', function(){
//     $posts = Post::where('users_count', '<', 50)->findOrFail();
//     return $posts;
// });


// Route::get('/basicinsert', function(){
//     $post = new Post;
//     $post->title = 'Again orm title';
//     $post->user_id = 2;
//     $post->content = 'New Eloquent is really cool, look at this bl bla';
//     $post->save();
// });

// //---------------------------------------CREATE--------------------------------------------
// Route::get('/basicupdate', function(){
//     $post = Post::find(2);
//     $post->title = 'New Eloquent title update';
//     $post->content = 'Wow eloquent is really cool, look at this contet 2';
//     $post->save();
// });

// Route::get('/create', function(){
//     //must set fillable property
//     Post::create([
//        'title'=>'The create Method',
//        'content'=> 'WOW i\'m learning a lot php with edvin'
//     ]);
// });

// //UPDATE
// Route::get('/update', function(){
//     //must set fillable property
//     Post::where('id', 2)->where('is_admin', 0)->update(
//         [
//             'title'=>'NEW PHP TITLE',
//             'content'=>'This is bren new content'
//         ]
//     );
// });

// //--------------------------------------- DELETE--------------------------------------------
// Route::get('/delete/{id}', function($id){
//     $post = Post::find($id);
//     $post->delete();
// });

// Route::get('/delete2/{id}', function($id){
//     // Post::destroy(5); //using for id

//     // Post::destroy([4,5]); //using for ids

//     Post::where('is_admin', 0)->delete();//using for conditions
// });


// //---------------------------------------SOFT DELETE--------------------------------------------
// //softdelete item
// Route::get('/softdelete', function(){
//     Post::find(3)->delete();
    
// });
// //Return only items whitch is not softdeleted
// Route::get('/readafterdelete', function(){
//     $posts = Post::all();
//     foreach($posts as $post){
//          echo"<pre>".print_r($post)."</pre>";
//     }
// });

// //Return only items who is softdeleted
// Route::get('/readsoftdelete', function(){
//     //$posts = Post::withTrashed()->where('id', 3)->get(); //get one deleted item
//     $posts = Post::withTrashed()->get();//get all delted items
//     return $posts;
// });

// //Restore softdeleted item in database
// Route::get('/restoresoftdeleteitems', function(){
//     Post::withTrashed()->where('id', 3)->restore();
// });

// //Hard delete softdeleted item from database
// Route::get('/forcedelete', function(){
//     Post::onlyTrashed()->where('id', 3)->forceDelete();
// });


// //------------------------------------------------------ ELOQUENT RELATIONSHIPS -------------------------------------------------------------------------
// //one to one relationship
// Route::get('/user/{id}/post/', function($id){
//     return User::find($id)->post;
// });

// //one to one relationship inverse
// Route::get('/post/{id}/user/', function($id){
//     return post::find($id)->user;
// });

// //one to meny relationship
// Route::get('/posts', function(){
//    $user = User::find(1);
//    $user->userPosts = $user->posts;
//    return $user;
//    //echo"<pre>".var_dump($user)."</pre>";
//    die;
//     foreach($user->posts as $post){

//     }
// });

// //many to many relationships
// Route::get('/user/{id}/roles', function($id){
//     $user = User::find($id);
//     $user->roles; 
//     return $user;
//  });

//  //Accessing the pivot/ intermidiate table
//  Route::get('/user/pivot', function(){
//     $user = User::find(1);
//     $arr = array();
//     foreach($user->roles as $role){
//         $arr[] = $role->pivot;
//     }
//     return $arr;
//  });

//  Route::get('/user/country', function(){
//     $country = Country::find(3);
//     //echo'<pre>'.$country->posts.'</pre>';
//     foreach($country->posts as $post){
//         echo'<pre>'.$post->title.'</pre>';
//     }
//  });

//  //------------------------------------------------------ POLYMORPHIC RELATIONS -------------------------------------------------------------------------

//  Route::get('/user/photos', function(){
//     $user = User::find(1);
//     return $user->photos;
//     foreach($user->photos as $photo){
//         $photos[] = $photo;
//     }
   
//  } );
//  Route::get('/post/photos', function(){
//     $post = User::find(1);
//     return $post->photos;
//     // foreach($post->photos as $photo){
//     //     $photos[] = $photo;
//     // }
   
//  } );
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
|--------------------------------------------------------------------------
| CRUD Application
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/posts', 'PostsController');

Route::group(['middleware'=>['web']], function(){});