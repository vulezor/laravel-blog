<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //one to one
    public function post(){
        return $this->hasOne('App\Post'); //second parametar sending if is conection to id is not in this case user_id
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function roles(){
        //To customize tables name and columns if not following convenction
        //return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
        return $this->belongsToMany('App\Role')->withPivot('created_at');//pivot by default gives only user_id and role_id.Other column to bi visible must set in withPivot method
    }

    public function photos(){
        return $this->morphMany('App\Photo', 'imageable');
    }

}
