<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Post extends Model
{
    use SoftDeletes;
    //
    // protected $table = 'posts';
    // protected $primaryKey = 'id'; //overide from model class

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title',
        'content'
    ];

    //one to one inverse
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function photos(){
        return $this->morphMany('App\Photo', 'imageable');
    }
}
